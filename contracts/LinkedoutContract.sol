pragma solidity >=0.4.21 <0.6.0;
pragma experimental ABIEncoderV2;

contract LinkedoutContract {
    struct Employee {
        string firstName;
        string lastName;
    }

    struct Job {
        string jobId;
        string name;
        uint256 startDate;
        uint256 endDate;
        Company company;
    }

    struct Training {
        string trainingId;
        string name;
        string trainingCenter;
        uint256 startDate;
        uint256 endDate;
        bool certified;
    }

    struct PersonnalProject {
        string projectId;
        string name;
        uint256 startDate;
        uint256 endDate;
    }

    struct Company {
        string companyId;
        string name;
        string companyAddress;
    }

    mapping(address => Training[]) public trainings;
    mapping(address => Job[]) public jobs;
    mapping(address => PersonnalProject[]) public personnalProjects;
    mapping(string => address[]) public commendingEmployees;

    function startJob(address employee, Job memory job) public {
        jobs[employee].push(job);
    }

    function endJob(address employee, Job memory job, uint256 endDate) public {
        for (uint i = 0; i < jobs[employee].length; i++) {
            if (compareStrings(jobs[employee][i].jobId, job.jobId)) {
                Job[] memory employeeJobs = jobs[employee];
                Job memory jobToUpdate = employeeJobs[i];
                jobToUpdate.endDate = endDate;
            }
        }
    }

    function addTraining(address employee, Training memory training) public {
        trainings[employee].push(training);
    }

    function addPersonnalProject(address employee, PersonnalProject memory personnalProject) public {
        personnalProjects[employee].push(personnalProject);
    }

    function commend(address commendingEmployee, PersonnalProject memory project) public {
        commendingEmployees[project.projectId].push(commendingEmployee);
    }

    function thankCommender(address payable commendingEmployee) public payable {
        commendingEmployee.send(msg.value);
    }

    function compareStrings(string memory a, string memory b) public view returns (bool) {
        return (keccak256(abi.encodePacked((a))) == keccak256(abi.encodePacked((b))));
    }
}